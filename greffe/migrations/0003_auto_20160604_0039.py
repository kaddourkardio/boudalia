# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('greffe', '0002_auto_20160603_1606'),
    ]

    operations = [
        migrations.AlterField(
            model_name='donneur',
            name='adresse',
            field=models.ForeignKey(null=True, to='greffe.Wilaya'),
        ),
        migrations.AlterField(
            model_name='donneur',
            name='alat',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='ALAT'),
        ),
        migrations.AlterField(
            model_name='donneur',
            name='anti_tpo',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Anti-corps anti TPO'),
        ),
        migrations.AlterField(
            model_name='donneur',
            name='asat',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='ASAT'),
        ),
        migrations.AlterField(
            model_name='donneur',
            name='groupage',
            field=models.CharField(max_length=1, choices=[('0', 'O rh +'), ('1', 'O rh -'), ('2', 'A rh +'), ('3', 'A rh -'), ('4', 'B rh +'), ('5', 'B rh -'), ('6', 'AB rh +'), ('7', 'AB rh -')], null=True, blank=True, verbose_name='Groupage sanguin'),
        ),
        migrations.AlterField(
            model_name='donneur',
            name='homa',
            field=models.FloatField(blank=True, null=True, verbose_name='indice HOMA'),
        ),
        migrations.AlterField(
            model_name='donneur',
            name='insulinemie',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Insulinémie'),
        ),
        migrations.AlterField(
            model_name='donneur',
            name='naissance',
            field=models.DateField(blank=True, null=True, verbose_name='date de naissance'),
        ),
        migrations.AlterField(
            model_name='donneur',
            name='nombre_enfants',
            field=models.PositiveSmallIntegerField(default=0, verbose_name="nombre d'enfants"),
        ),
        migrations.AlterField(
            model_name='donneur',
            name='profession',
            field=models.CharField(max_length=1, choices=[('C', 'Cadre, Administrateur'), ('E', 'Employé, Fonctionnaire'), ('M', 'Commerçant'), ('F', 'Femme au foyer'), ('S', 'étudiant, universitaire'), ('V', 'Ouvrier'), ('B', 'Ouvrier non déclaré'), ('R', 'Retraité'), ('H', 'Chomeur, inactif')], null=True, verbose_name='Profession'),
        ),
        migrations.AlterField(
            model_name='donneur',
            name='situation_sociale',
            field=models.CharField(max_length=1, choices=[('M', 'Marié'), ('C', 'Célibataire'), ('V', 'veuf'), ('D', 'Divorcé')], default='C', verbose_name='Situation sociale'),
        ),
        migrations.AlterField(
            model_name='donneur',
            name='uricemie',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Acide urique'),
        ),
        migrations.AlterField(
            model_name='evolution',
            name='alat',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='ALAT'),
        ),
        migrations.AlterField(
            model_name='evolution',
            name='anti_tpo',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Anti-corps anti TPO'),
        ),
        migrations.AlterField(
            model_name='evolution',
            name='asat',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='ASAT'),
        ),
        migrations.AlterField(
            model_name='evolution',
            name='autres_complications',
            field=models.CharField(max_length=255, blank=True, null=True, verbose_name='Autres coplications'),
        ),
        migrations.AlterField(
            model_name='evolution',
            name='bmi',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='BMI'),
        ),
        migrations.AlterField(
            model_name='evolution',
            name='creatinine',
            field=models.DecimalField(decimal_places=2, max_digits=5, blank=True, null=True, verbose_name='Créatininémie'),
        ),
        migrations.AlterField(
            model_name='evolution',
            name='homa',
            field=models.FloatField(blank=True, null=True, verbose_name='indice HOMA'),
        ),
        migrations.AlterField(
            model_name='evolution',
            name='insulinemie',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Insulinémie'),
        ),
        migrations.AlterField(
            model_name='evolution',
            name='ira',
            field=models.NullBooleanField(default=False, verbose_name="Episodes d'IRA"),
        ),
        migrations.AlterField(
            model_name='evolution',
            name='labstix',
            field=models.CharField(max_length=100, blank=True, null=True, verbose_name='Labstix'),
        ),
        migrations.AlterField(
            model_name='evolution',
            name='nephoropathie',
            field=models.NullBooleanField(default=False, verbose_name='Néphropathie decélée'),
        ),
        migrations.AlterField(
            model_name='evolution',
            name='pbr',
            field=models.NullBooleanField(default=False, verbose_name='PBR'),
        ),
        migrations.AlterField(
            model_name='evolution',
            name='poids',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Poids'),
        ),
        migrations.AlterField(
            model_name='evolution',
            name='proteinurie',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Protéinurie des 24h'),
        ),
        migrations.AlterField(
            model_name='evolution',
            name='recherche_ac',
            field=models.CharField(max_length=50, blank=True, null=True, verbose_name="type d'anti corps recherchés"),
        ),
        migrations.AlterField(
            model_name='evolution',
            name='recherche_pr',
            field=models.CharField(max_length=50, blank=True, null=True, verbose_name='PR'),
        ),
        migrations.AlterField(
            model_name='evolution',
            name='taille',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Taille'),
        ),
        migrations.AlterField(
            model_name='evolution',
            name='tour_taille',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Tour de taille'),
        ),
        migrations.AlterField(
            model_name='evolution',
            name='triglycerides',
            field=models.FloatField(blank=True, default=1.5, verbose_name='Triglycérides'),
        ),
        migrations.AlterField(
            model_name='evolution',
            name='uricemie',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Acide urique'),
        ),
        migrations.AlterField(
            model_name='greffe',
            name='centre',
            field=models.ForeignKey(null=True, to='greffe.Centre'),
        ),
        migrations.AlterField(
            model_name='greffe',
            name='ctc',
            field=models.CharField(max_length=50, blank=True, null=True, verbose_name='Corticoïdes'),
        ),
        migrations.AlterField(
            model_name='greffe',
            name='date_intervention',
            field=models.DateField(blank=True, null=True, verbose_name="Date de l'intervention"),
        ),
        migrations.AlterField(
            model_name='greffe',
            name='immuno',
            field=models.CharField(max_length=1, choices=[('C', 'CSA'), ('T', 'Tacro')], null=True, blank=True, verbose_name='CSA/tacro'),
        ),
        migrations.AlterField(
            model_name='greffe',
            name='immuno_poso',
            field=models.CharField(max_length=50, blank=True, null=True, verbose_name='Posologies'),
        ),
        migrations.AlterField(
            model_name='greffe',
            name='immunosuppression',
            field=models.CharField(max_length=1, choices=[('C', 'CSA'), ('T', 'Tacro')], null=True, blank=True, verbose_name='Azathioprime/MMF'),
        ),
        migrations.AlterField(
            model_name='greffe',
            name='immunosuppression_poso',
            field=models.CharField(max_length=50, blank=True, null=True, verbose_name='Posologies'),
        ),
        migrations.AlterField(
            model_name='greffe',
            name='ischemie_chaude',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name="Durée de l'ischémie chaude"),
        ),
        migrations.AlterField(
            model_name='greffe',
            name='ischemie_froide',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name="Durée de l'ischémie froide"),
        ),
        migrations.AlterField(
            model_name='greffe',
            name='protocole_induction',
            field=models.CharField(max_length=1, choices=[('A', 'ATG'), ('S', 'Simulect'), ('O', 'autre')], null=True, blank=True, verbose_name="Protocole d'induction"),
        ),
        migrations.AlterField(
            model_name='receveur',
            name='activite',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Activité physique'),
        ),
        migrations.AlterField(
            model_name='receveur',
            name='adresse',
            field=models.ForeignKey(null=True, to='greffe.Wilaya'),
        ),
        migrations.AlterField(
            model_name='receveur',
            name='alat',
            field=models.PositiveSmallIntegerField(default=20, verbose_name='ALAT'),
        ),
        migrations.AlterField(
            model_name='receveur',
            name='anti_tpo',
            field=models.PositiveSmallIntegerField(blank=True, default=0, verbose_name='Anti-corps anti TPO'),
        ),
        migrations.AlterField(
            model_name='receveur',
            name='asat',
            field=models.PositiveSmallIntegerField(default=20, verbose_name='ASAT'),
        ),
        migrations.AlterField(
            model_name='receveur',
            name='bmi',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='BMI'),
        ),
        migrations.AlterField(
            model_name='receveur',
            name='commentaire',
            field=models.CharField(max_length=140, blank=True, null=True, verbose_name='Commentaire'),
        ),
        migrations.AlterField(
            model_name='receveur',
            name='compatibilite_hla',
            field=models.CharField(max_length=1, choices=[('0', 'Geno identiques'), ('1', 'Haplo identiques'), ('2', 'différents')], default=1, blank=True, verbose_name='Compatibilité HLA'),
        ),
        migrations.AlterField(
            model_name='receveur',
            name='date_debut',
            field=models.DateField(blank=True, null=True, verbose_name='Date de première dialyse'),
        ),
        migrations.AlterField(
            model_name='receveur',
            name='date_grossesse',
            field=models.DateField(blank=True, null=True, verbose_name='Date de denière grossesse'),
        ),
        migrations.AlterField(
            model_name='receveur',
            name='date_transfusion',
            field=models.DateField(blank=True, null=True, verbose_name='Date de denière transfusion'),
        ),
        migrations.AlterField(
            model_name='receveur',
            name='dose_epo',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name="Dose d'EPO en unité/semaine"),
        ),
        migrations.AlterField(
            model_name='receveur',
            name='groupage',
            field=models.CharField(max_length=1, choices=[('0', 'O rh +'), ('1', 'O rh -'), ('2', 'A rh +'), ('3', 'A rh -'), ('4', 'B rh +'), ('5', 'B rh -'), ('6', 'AB rh +'), ('7', 'AB rh -')], null=True, blank=True, verbose_name='Groupage sanguin'),
        ),
        migrations.AlterField(
            model_name='receveur',
            name='homa',
            field=models.FloatField(blank=True, null=True, verbose_name='indice HOMA'),
        ),
        migrations.AlterField(
            model_name='receveur',
            name='insulinemie',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Insulinémie'),
        ),
        migrations.AlterField(
            model_name='receveur',
            name='naissance',
            field=models.DateField(blank=True, null=True, verbose_name='date de naissance'),
        ),
        migrations.AlterField(
            model_name='receveur',
            name='nephropathie_initiale',
            field=models.ForeignKey(to='greffe.Nephropathie', blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='receveur',
            name='nombre_enfants',
            field=models.PositiveSmallIntegerField(default=0, verbose_name="nombre d'enfants"),
        ),
        migrations.AlterField(
            model_name='receveur',
            name='nombre_grossesses',
            field=models.PositiveSmallIntegerField(null=True, blank=True, default=0, verbose_name='Nombre de grossesses'),
        ),
        migrations.AlterField(
            model_name='receveur',
            name='nombre_mismatch',
            field=models.PositiveSmallIntegerField(blank=True, default=1, verbose_name='Nombre de mismatch'),
        ),
        migrations.AlterField(
            model_name='receveur',
            name='nombre_transfusions',
            field=models.PositiveSmallIntegerField(blank=True, default=0, verbose_name='Nombre de transfusions'),
        ),
        migrations.AlterField(
            model_name='receveur',
            name='pathologie_associee',
            field=models.CharField(max_length=255, null=True, verbose_name='Pathologies associées'),
        ),
        migrations.AlterField(
            model_name='receveur',
            name='poids',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Poids'),
        ),
        migrations.AlterField(
            model_name='receveur',
            name='taille',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Taille'),
        ),
        migrations.AlterField(
            model_name='receveur',
            name='tour_taille',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Tour de taille'),
        ),
        migrations.AlterField(
            model_name='receveur',
            name='transfusions_post',
            field=models.PositiveSmallIntegerField(blank=True, default=0, verbose_name='Nombre de transfusions post-greffe'),
        ),
        migrations.AlterField(
            model_name='receveur',
            name='triglycerides',
            field=models.FloatField(blank=True, null=True, verbose_name='Triglycérides'),
        ),
        migrations.AlterField(
            model_name='receveur',
            name='uricemie',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Acide urique'),
        ),
    ]
