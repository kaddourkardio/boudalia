# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('greffe', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='receveur',
            old_name='Nephropathie_initiale',
            new_name='nephropathie_initiale',
        ),
    ]
