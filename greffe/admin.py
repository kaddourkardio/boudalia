from django.contrib import admin
from . import models

# Register your models here.
class ReceveurAdmin(admin.ModelAdmin):
    list_display = ("numero", "nom_receveur", "prenom_receveur","naissance", "phone", "adresse")
    list_filter = ('nom_receveur', 'sexe', 'assurance', 'diabete', 'type_dialyse','compatibilite_hla' )
    search_fields = ('numero','nom_receveur', 'prenom_receveur', 'phone')


class GreffeAdmin(admin.ModelAdmin):
    list_display = ("receveur", "donneur", "date_intervention","centre")
    search_fields = ('receveur','date_intervention')


class EvolutionAdmin(admin.ModelAdmin):
    list_display = ("receveur", "visite","ira", "pbr")
    list_filter = ('receveur', 'visite')
    search_fields = ('receveur',)

# admin.site.register(models.Patient, PatientAdmin)
admin.site.register(models.Wilaya)
admin.site.register(models.Centre)
admin.site.register(models.Nephropathie)
admin.site.register(models.Donneur)
admin.site.register(models.Receveur, ReceveurAdmin)
admin.site.register(models.Greffe, GreffeAdmin)
admin.site.register(models.Evolution, EvolutionAdmin)

