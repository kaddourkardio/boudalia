from django.db import models
from datetime import datetime
from django.utils import timezone
from django.conf import settings
from django.core.urlresolvers import reverse


class Wilaya(models.Model):
    wilaya = models.CharField(max_length = 25)
    code = models.PositiveSmallIntegerField("code")

    def __str__(self):
            return(self.wilaya)


class Centre(models.Model):
    centre = models.CharField("Unité de transplantation", max_length = 25)
    def __str__(self):
            return(self.centre)


class Nephropathie(models.Model):
    nephropathie = models.CharField(verbose_name="Néphoropathie", max_length = 50)

    def __str__(self):
            return(self.nephropathie)


class Tag(models.Model):
    tag = models.CharField(max_length = 50, blank=True)

    def __str__(self):
            return(self.tag)


                 # operateur = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True)
                 #                 aide = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True, related_name='intervention_coronarographie_aide')
                 #                 emergency = models.BooleanField("urgence", default=False)
                 #                 indication = models.ManyToManyField(Indication, blank=True)
                 #                     ordering = ['name']

#                 def get_absolute_url(self):
#                     return reverse('detail_patient', kwargs={'pk': self.pk})

#                 def __str__(self):
#                     return(self.name)

class Receveur(models.Model):
    numero = models.PositiveSmallIntegerField(unique=True, blank=True)
    nom_receveur = models.CharField("Nom du receveur", max_length = 50)
    prenom_receveur = models.CharField("Prénoms du receveur", max_length = 50)
    naissance = models.DateField("date de naissance", blank=True, null=True)
    GENDER_CHOICES = (
                                ('M', 'Masculin'),
                                ('F', 'Féminin'),
                )
    sexe = models.CharField(verbose_name="Sexe", max_length=1, choices=GENDER_CHOICES)
    adresse = models.ForeignKey(Wilaya, null=True)
    phone = models.CharField("Téléphone", max_length = 10, default='0550123456')
    assurance = models.BooleanField(verbose_name="assuré social", default=True)
    MARITAL_CHOICES = (
                                ('M', 'Marié'),
                                ('C', 'Célibataire'),
                                ('V', 'veuf'),
                                ('D', 'Divorcé'),
                )
    situation_sociale = models.CharField("Situation sociale", max_length = 1, choices=MARITAL_CHOICES)
    nombre_enfants = models.PositiveSmallIntegerField("nombre d'enfants", default=0)
    PROFESSION_CHOICES = (
                                ('C', 'Cadre, Administrateur'),
                                ('E', 'Employé, Fonctionnaire'),
                                ('M', 'Commerçant'),
                                ('F', 'Femme au foyer'),
                                ('S', 'étudiant, universitaire'),
                                ('V', 'Ouvrier'),
                                ('B', 'Ouvrier non déclaré'),
                                ('R', 'Retraité'),
                                ('H', 'Chomeur, inactif'),

    )
    profession = models.CharField("Profession", max_length = 1, choices=PROFESSION_CHOICES)
    inactif_depuis = models.PositiveSmallIntegerField("nombre d'années d'inactivité", default="0")
    NIVEAU_CHOICES = (
                                ('I', 'illetré'),
                                ('S', 'sans diplôme'),
                                ('M', 'BEM'),
                                ('B', 'Baccalauréat'),
                                ('L', 'Licence'),
                                ('D', 'Doctorat'),
                                ('A', 'Master'),

    )
    niveau = models.CharField("Niveau d'études", max_length = 1, choices=NIVEAU_CHOICES)
    HABITATION_CHOICES = (
                                ('P', 'propriétaire'),
                                ('L', 'locataire'),
                                ('H', 'hébergé chez un parent du premier degré'),
                                ('T', 'hébergé chez un tiers'),

    )
    habitation = models.CharField("Mode d'habitation", max_length = 1, choices=HABITATION_CHOICES)
    activite = models.PositiveSmallIntegerField("Activité physique", blank=True, null=True)
    hta = models.BooleanField("HTA", default=False)
    obesite= models.BooleanField("Obésité", default=False)
    cardiovx = models.BooleanField("Maladie cardio vasculaire", default=False)
    tabac = models.BooleanField("Tabagisme", default=False)
    DIABET_CHOICES = (
                                ('0', 'non diabétique'),
                                ('1', 'Type 1'),
                                ('2', 'Type 2'),

    )
    diabete = models.CharField("Diabète", max_length = 1, choices=DIABET_CHOICES, default="0")
    taille = models.PositiveSmallIntegerField("Taille", blank=True, null=True)
    poids = models.PositiveSmallIntegerField("Poids", blank=True, null=True)
    tour_taille = models.PositiveSmallIntegerField("Tour de taille", blank=True, null=True)
    bmi = models.PositiveSmallIntegerField("BMI", blank=True, null=True)
    nephropathie_initiale = models.ForeignKey(Nephropathie, blank=True, null=True)
    pathologie_associee = models.CharField("Pathologies associées", max_length = 255, null=True)
    nigricans = models.BooleanField("Acanthosis nigricans", default=False)
    goitre = models.BooleanField("Goitre", default=False)
    DIALYSE_CHOICES = (
                                ('0', 'non précisé'),
                                ('1', 'Hémodialyse'),
                                ('2', 'Dialyse péritonéale'),
                )
    type_dialyse = models.CharField("Type de dialyse", max_length = 1, choices=DIALYSE_CHOICES, default='1')
    date_debut = models.DateField("Date de première dialyse", blank=True, null=True)
    commentaire = models.CharField("Commentaire", max_length = 140, blank=True, null=True)
    dose_epo = models.PositiveSmallIntegerField("Dose d'EPO en unité/semaine", blank=True, null=True)
    prelevement = models.CharField("Prélèvement numéro ", max_length=50, null=True)
    crp = models.PositiveSmallIntegerField("CRP", default='0')
    hemoglobine = models.DecimalField("Hémoglobine", max_digits=5, decimal_places=2, default=9)

    GROUPAGE_CHOICES = (
                                ('0', 'O rh +'),
                                ('1', 'O rh -'),
                                ('2', 'A rh +'),
                                ('3', 'A rh -'),
                                ('4', 'B rh +'),
                                ('5', 'B rh -'),
                                ('6', 'AB rh +'),
                                ('7', 'AB rh -')
                )

    groupage = models.CharField("Groupage sanguin", max_length = 1, choices=GROUPAGE_CHOICES, blank=True, null=True)
    cmv = models.NullBooleanField("sérologie CMV", default = False)
    ebv = models.NullBooleanField("sérologie EBV", default = False)
    hbs = models.NullBooleanField("sérologie HBS", default = False)
    hiv = models.NullBooleanField("sérologie HIV", default = False)
    hcv = models.NullBooleanField("sérologie HCV", default = False)
    glycemie = models.DecimalField("Glycémie", max_digits=4, decimal_places=2, default=1.00)

    triglycerides = models.FloatField("Triglycérides", blank=True, null=True)
    glycemie = models.DecimalField("Glycémie", max_digits=4, decimal_places=2, default=1.00)
    cholesterol = models.DecimalField("Cholesterol total", max_digits=4, decimal_places=2, default=1.00)
    hdl = models.DecimalField("HDL", max_digits=4, decimal_places=2, default=1.00)
    ldl = models.DecimalField("LDL", max_digits=4, decimal_places=2, default=1.00)
    hba1c = models.DecimalField("HbA1C", max_digits=4, decimal_places=2, default=7.00)
    alat = models.PositiveSmallIntegerField("ALAT", default=20)
    asat = models.PositiveSmallIntegerField("ASAT", default=20)

    uricemie = models.PositiveSmallIntegerField("Acide urique", blank=True, null=True)
    hgpo = models.DecimalField("HGPO à H2", max_digits=4, decimal_places=2, default=1.00)
    insulinemie = models.PositiveSmallIntegerField("Insulinémie", blank=True, null=True)
    homa = models.FloatField("indice HOMA", blank=True, null=True)
    tsh = models.DecimalField("TSHus", max_digits=5, decimal_places=2, default=1.00)
    anti_tpo = models.PositiveSmallIntegerField("Anti-corps anti TPO", blank=True, default=0)
    #  HLA faire des champs de cherfield avec 4 caractères.
    hla_a1 = models.CharField("HLA A", max_length=4, null=True)
    hla_a2 = models.CharField("HLA A bis", max_length=4, null=True)
    hla_b1 = models.CharField("HLA B", max_length=4, null=True)
    hla_b2 = models.CharField("HLA B bis", max_length=4, null=True)
    hla_dq1 = models.CharField("HLA DQ", max_length=4, null=True)
    hla_dq2 = models.CharField("HLA DQ bis", max_length=4, null=True)
    hla_dr1 = models.CharField("HLA DR", max_length=4, null=True)
    hla_dr2 = models.CharField("HLA DR bis", max_length=4, null=True)








    COMPATIBILITE_CHOICES = (

                     ('0', 'Geno identiques'),
                    ('1', 'Haplo identiques'),
                    ('2', 'différents')

    )

    compatibilite_hla = models.CharField("Compatibilité HLA", max_length = 1, choices=COMPATIBILITE_CHOICES, default=1, blank=True)
    nombre_mismatch = models.PositiveSmallIntegerField("Nombre de mismatch", blank=True, default=1)
    nombre_transfusions = models.PositiveSmallIntegerField("Nombre de transfusions", blank=True, default=0)
    nombre_grossesses = models.PositiveSmallIntegerField("Nombre de grossesses", blank=True, null=True, default=0)
    greffe_antrieure = models.NullBooleanField("Greffe antérieure", default=False)
    transfusions_post = models.PositiveSmallIntegerField("Nombre de transfusions post-greffe", blank=True, default=0)
    date_transfusion = models.DateField("Date de denière transfusion", blank=True, null=True)

    date_grossesse= models.DateField("Date de denière grossesse", blank=True, null=True)





    class Meta:
        ordering = ['numero']

#                def get_absolute_url(self):
#                    return reverse('detail_patient', kwargs={'pk': self.pk})

    def __str__(self):
        return '%s %s %s' % (self.nom_receveur, self.prenom_receveur, self.numero)


class Donneur(models.Model):

    nom_donneur = models.CharField("Nom du donneur", max_length = 50)
    prenom_donneur = models.CharField("Prénoms du donneur", max_length = 50)
    naissance = models.DateField("date de naissance", blank=True, null=True)
    GENDER_CHOICES = (
                    ('M', 'Masculin'),
                    ('F', 'Féminin'),
    )
    sexe = models.CharField(verbose_name="Sexe", max_length = 1, choices=GENDER_CHOICES)
    adresse = models.ForeignKey(Wilaya, null=True)
    phone = models.CharField("Téléphone", max_length = 10, default='0550123456')
    assurance = models.BooleanField(verbose_name="assuré social", default=True)
    MARITAL_CHOICES = (
                    ('M', 'Marié'),
                    ('C', 'Célibataire'),
                    ('V', 'veuf'),
                    ('D', 'Divorcé'),
    )
    situation_sociale = models.CharField("Situation sociale", max_length = 1, choices=MARITAL_CHOICES, default='C')
    nombre_enfants = models.PositiveSmallIntegerField("nombre d'enfants", default=0)
    PROFESSION_CHOICES = (
                    ('C', 'Cadre, Administrateur'),
                    ('E', 'Employé, Fonctionnaire'),
                    ('M', 'Commerçant'),
                    ('F', 'Femme au foyer'),
                    ('S', 'étudiant, universitaire'),
                    ('V', 'Ouvrier'),
                    ('B', 'Ouvrier non déclaré'),
                    ('R', 'Retraité'),
                    ('H', 'Chomeur, inactif'),

    )
    profession = models.CharField("Profession", max_length = 1, choices=PROFESSION_CHOICES, null=True)

    ##  HLA faire des champs de cherfield avec 4 caractères.
    hla_a1 = models.CharField("HLA A", max_length=4, null=True)
    hla_a2 = models.CharField("HLA A bis", max_length=4, null=True)
    hla_b1 = models.CharField("HLA B", max_length=4, null=True)
    hla_b2 = models.CharField("HLA B bis", max_length=4, null=True)
    hla_dq1 = models.CharField("HLA DQ", max_length=4, null=True)
    hla_dq2 = models.CharField("HLA DQ bis", max_length=4, null=True)
    hla_dr1 = models.CharField("HLA DR", max_length=4, null=True)
    hla_dr2 = models.CharField("HLA DR bis", max_length=4, null=True)

    GROUPAGE_CHOICES = (
                    ('0', 'O rh +'),
                    ('1', 'O rh -'),
                    ('2', 'A rh +'),
                    ('3', 'A rh -'),
                    ('4', 'B rh +'),
                    ('5', 'B rh -'),
                    ('6', 'AB rh +'),
                    ('7', 'AB rh -')
    )

    groupage = models.CharField("Groupage sanguin", max_length = 1, choices=GROUPAGE_CHOICES, blank=True, null=True)
    glycemie = models.DecimalField("Glycémie", max_digits=4, decimal_places=2, default=1.00)

    triglycerides = models.FloatField("Triglycérides", blank=True)
    glycemie = models.DecimalField("Glycémie", max_digits=4, decimal_places=2, default=1.00)
    cholesterol = models.DecimalField("Cholesterol total", max_digits=4, decimal_places=2, default=1.00)
    hdl = models.DecimalField("HDL", max_digits=4, decimal_places=2, default=1.00)
    ldl = models.DecimalField("LDL", max_digits=4, decimal_places=2, default=1.00)
    hba1c = models.DecimalField("HbA1C", max_digits=4, decimal_places=2, default=7.00)
    alat = models.PositiveSmallIntegerField("ALAT", blank=True, null=True)
    asat = models.PositiveSmallIntegerField("ASAT", blank=True, null=True)

    uricemie = models.PositiveSmallIntegerField("Acide urique", blank=True, null=True)
    hgpo = models.DecimalField("HGPO à H2", max_digits=4, decimal_places=2, default=1.00)
    insulinemie = models.PositiveSmallIntegerField("Insulinémie", blank=True, null=True)
    homa = models.FloatField("indice HOMA", blank=True, null=True)
    tsh = models.DecimalField("TSHus", max_digits=5, decimal_places=2, default=1.00)
    anti_tpo = models.PositiveSmallIntegerField("Anti-corps anti TPO", blank=True, null=True)


    class Meta:
        ordering = ['nom_donneur']

#                def get_absolute_url(self):
#                    return reverse('detail_donneur', kwargs={'pk': self.pk})

    def __str__(self):
        return '%s %s' % (self.nom_donneur, self.prenom_donneur)


class Evolution(models.Model):
    receveur = models.ForeignKey(Receveur)
    RECUL_CHOICES = (
                                ('1', 'J30'),
                                ('2', 'J180'),
                )
    visite = models.CharField(verbose_name="Visite", max_length = 1, choices=RECUL_CHOICES)
    taille = models.PositiveSmallIntegerField("Taille", blank=True, null=True)
    poids = models.PositiveSmallIntegerField("Poids", blank=True, null=True)
    tour_taille = models.PositiveSmallIntegerField("Tour de taille", blank=True, null=True)
    bmi = models.PositiveSmallIntegerField("BMI", blank=True, null=True)
    prelevement = models.CharField("Prélèvement numéro ", max_length=50, null=True)
    creatinine = models.DecimalField("Créatininémie", max_digits=5, decimal_places=2, blank=True, null=True)
    glycemie = models.DecimalField("Glycémie", max_digits=4, decimal_places=2, default=1.00)

    triglycerides = models.FloatField("Triglycérides", blank=True, default=1.50)
    glycemie = models.DecimalField("Glycémie", max_digits=4, decimal_places=2, default=1.00)
    cholesterol = models.DecimalField("Cholesterol total", max_digits=4, decimal_places=2, default=1.00)
    hdl = models.DecimalField("HDL", max_digits=4, decimal_places=2, default=1.00)
    ldl = models.DecimalField("LDL", max_digits=4, decimal_places=2, default=1.00)
    hba1c = models.DecimalField("HbA1C", max_digits=4, decimal_places=2, default=7.00)
    alat = models.PositiveSmallIntegerField("ALAT", blank=True, null=True)
    asat = models.PositiveSmallIntegerField("ASAT", blank=True, null=True)

    uricemie = models.PositiveSmallIntegerField("Acide urique", blank=True, null=True)
    hgpo = models.DecimalField("HGPO à H2", max_digits=4, decimal_places=2, default=1.00)
    insulinemie = models.PositiveSmallIntegerField("Insulinémie", blank=True, null=True)
    homa = models.FloatField("indice HOMA", blank=True, null=True)
    tsh = models.DecimalField("TSHus", max_digits=5, decimal_places=2, default=1.00)
    anti_tpo = models.PositiveSmallIntegerField("Anti-corps anti TPO", blank=True, null=True)
   
    proteinurie = models.PositiveSmallIntegerField("Protéinurie des 24h", blank=True, null=True)
    labstix = models.CharField("Labstix", max_length = 100, blank=True, null=True)
    ira = models.NullBooleanField("Episodes d'IRA", default=False)
    nephoropathie = models.NullBooleanField("Néphropathie decélée", default=False)
    pbr = models.NullBooleanField("PBR", default=False)

    autres_complications = models.CharField("Autres coplications", max_length = 255, blank=True, null=True)
    recherche_ac = models.CharField("type d'anti corps recherchés", max_length = 50, blank=True, null=True)
    recherche_pr = models.CharField("PR", max_length = 50, blank=True, null=True)

    class Meta:
        ordering = ['receveur']

#                def get_absolute_url(self):
#                    return reverse('detail_evolution', kwargs={'pk': self.pk})

    def __str__(self):
            return '%s - %s' % (self.receveur, self.visite)
     
class Greffe(models.Model):
    donneur = models.ForeignKey(Donneur, blank=True)
    receveur = models.ForeignKey(Receveur, blank=True)
    date_intervention = models.DateField("Date de l'intervention", blank=True, null=True)
    centre = models.ForeignKey(Centre, null=True)
    ischemie_chaude = models.PositiveSmallIntegerField("Durée de l'ischémie chaude", blank=True, null=True)
    ischemie_froide = models.PositiveSmallIntegerField("Durée de l'ischémie froide", blank=True, null=True)

    INDUCTION_CHOICES = (

                     ('A', 'ATG'),
                    ('S', 'Simulect'),
                    ('O', 'autre')

    )

    protocole_induction = models.CharField("Protocole d'induction", max_length = 1, choices=INDUCTION_CHOICES, blank=True, null=True)
    ctc = models.CharField("Corticoïdes", max_length=50, blank=True, null=True)
    IMMUNO_CHOICES = (

                     ('C', 'CSA'),
                    ('T', 'Tacro'),


    )

    immuno = models.CharField("CSA/tacro", max_length=1, choices=IMMUNO_CHOICES, blank=True, null=True)
    immuno_poso = models.CharField("Posologies", max_length = 50, blank=True, null=True)
    IMMUNOSUPPRESSION_CHOICES = (

                     ('A', 'Azathioprime'),
                    ('M', 'MMF'),


    )

    immunosuppression = models.CharField("Azathioprime/MMF", max_length = 1, choices=IMMUNOSUPPRESSION_CHOICES, blank=True, null=True)
    immunosuppression_poso = models.CharField("Posologies", max_length = 50, blank=True, null=True)

    class Meta:
        ordering = ['-date_intervention']

#                def get_absolute_url(self):
#                    return reverse('detail_greffe', kwargs={'pk': self.pk})

    def __str__(self):
            return '%s - %s' % (self.receveur, self.date_intervention)

