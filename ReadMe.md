# Bienvenus dans l'application du Docteur **M.A.Boudalia**

Cette application servira à la collecte d'information pour le registre de **greffe rénale** des hôpitaux d'Alger (et d'ailleurs).

## Objectifs

- Faciliter la collecte de données.
- Garantir la pérennité et la securité des archives médicaux
- Aide précieuse pour la recherche scientifique.
- Automatiser la création de rapports d'activité.

## TODO

- Beaucoup de choses ...

## Crédits

Ceci est le fruit de la collaboration du Dr Boudalia Amir, néphrologue au CHU Mustapha, et du [Dr Yahyaoui Kaddour](https://tuxador.github.io) , cardiologue.

[![Built with Spacemacs](https://cdn.rawgit.com/syl20bnr/spacemacs/442d025779da2f62fc86c2082703697714db6514/assets/spacemacs-badge.svg)](http://github.com/syl20bnr/spacemacs)